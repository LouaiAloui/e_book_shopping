﻿using domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class Item
    {
        private int id;
        private Produit pr = new Produit();
        private int quantity;
        private string image;
        public Item(){}
       

        
       
        
        public Item(int id, Produit product, int quantity, string image)
        {
            this.id = product.idProduit;
            this.pr = product;
            this.quantity = quantity;
            this.imagePath = image;
        }

        public Produit Pr { get => pr; set => pr = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public int Id { get => id; set => id = value; }
        public string imagePath { get => image; set => image = value; }
    }
}