﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using data;
using domain;
using service;

namespace web.Controllers
{
    public class StatuController : Controller
    {
        
        private PidevContext db = new PidevContext();

        
        // GET: Statu
        public ActionResult Index()
        {
           
            return View(db.Status.Include("userOfStat").Include("listComByStat.userCom").ToList());
        }

        public ActionResult Create()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        public int AddStatu(string subject)
        { 
            Statu s = new Statu();
            try
            {
                if (Session["idu"] != null)
                {
                    //s.iStatu = 6;
                    s.sujet = subject;
                    s.datStat = DateTime.Now;
                    int iduser = Convert.ToInt32(Session["idu"]);
                    User u = db.Users.Find(iduser);
                    
                    if (u != null)
                    {
                        s.userOfStat = u;
                        db.Status.Add(s);
                        db.SaveChanges();  
                    }
                }
            }
            catch (DbEntityValidationException e)
            {}
            return 1;
        }

        [HttpPost]
        public int AddComToStat(int id,String subject)
        {
            try
            {
                Commentaire c = new Commentaire();

                if (Session["idu"] != null)
                {
                    Statu s = db.Status.Find(id);
                    if (s != null)
                    {
                        c.forStatu = s;
                    }
                    c.sujetCom = subject;
                    c.datCom = DateTime.Now;
                    int iduser = Convert.ToInt32(Session["idu"]);
                    User u = db.Users.Find(iduser);

                    c.userCom = u;
                    db.Commentaires.Add(c);
                    db.SaveChanges();

                }
            }
            catch
            {
                
            }
            
            return 1;
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
