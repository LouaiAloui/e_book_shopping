﻿using data;
using domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
namespace web.Controllers
{
    public class ShoppingCartController : Controller
    {
        PidevContext db = new PidevContext();
        // GET: ShoppingCart
        public ActionResult Index()
        {

            return View();
        }
        private int isExisting(int id)
        {
            List<Item> cart = (List<Item>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
                if (cart[i].Pr.idProduit == id)
                    return i;
            return -1;
        }
        public ActionResult Delete(int idProd)
        {
            List<Item> cart = (List<Item>)Session["cart"];
            
            if (Session["cart"] != null)
            {
                int index = isExisting(idProd);
                Item item = new Item();
                if (index != -1)
                {
                    if (cart[index].Quantity > 1)
                    {
                        cart[index].Quantity--;
                    }
                    else
                    {
                        cart.RemoveAt(index);
                    } 
                }
                Session["cart"] = cart;
            }
             
            return RedirectToAction("Index", "Produit");
        }
        public ActionResult OrderNow(int id)
        {
            if(Session["cart"] == null)
            {
                
                List<Item> cart = new List<Item>();
                cart.Add(new Item(id,db.Produits.Find(id), 1, db.Produits.Find(id).imagePath));
                Session["cart"] = cart;
            }
            else
            {
                List<Item> cart =(List<Item>)Session["cart"];
                int index = isExisting(id);
                if (index == -1)
                    cart.Add(new Item(id,db.Produits.Find(id), 1, db.Produits.Find(id).imagePath));
                else
                     
                    cart[index].Quantity++;
                
                Session["cart"] = cart;
            }
            return RedirectToAction("Index", "Produit");
            //return "ok";
            //return View("Cart");
        } 
    }
}