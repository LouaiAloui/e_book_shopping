﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using data;
using domain;
using System.IO;

namespace web.Controllers
{
    public class ProduitBackController : Controller
    {
        private PidevContext db = new PidevContext();

        // GET: ProduitBack
        public ActionResult Index()
        {
           

            return View(db.Produits.ToList());
        }

        // GET: ProduitBack/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produit produit = db.Produits.Find(id);
            if (produit == null)
            {
                return HttpNotFound();
            }
            return View(produit);
        }

        // GET: ProduitBack/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProduitBack/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idProduit,prix,description,dateProd,categorie,FileName,FileContent,Files,image,imagePath")] Produit produit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    String FileExt = Path.GetExtension(produit.Files.FileName).ToUpper();
                    if (FileExt == ".PDF")
                    {
                        Byte[] data = new byte[produit.Files.ContentLength];
                        produit.Files.InputStream.Read(data, 0, produit.Files.ContentLength);
                        produit.FileName = produit.Files.FileName;
                        produit.FileContent = data;
                    }
                    string fileName = Path.GetFileNameWithoutExtension(produit.image.FileName);
                    string extension = Path.GetExtension(produit.image.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff")+extension;
                    produit.imagePath = "~/Image/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                    produit.image.SaveAs(fileName);

                    db.Produits.Add(produit);
                    db.SaveChanges();
                    db.SaveChanges();

                    ModelState.Clear();
                    return RedirectToAction("Index");

                }
            }
            catch
            {

            }
            
            return View(produit);
        }

        // GET: ProduitBack/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produit produit = db.Produits.Find(id);
            if (produit == null)
            {
                return HttpNotFound();
            }
            return View(produit);
        }

        // POST: ProduitBack/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idProduit,prix,description,dateProd,categorie,FileName,FileContent,Files,image,imagePath")] Produit produit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    String FileExt = Path.GetExtension(produit.Files.FileName).ToUpper();
                    if (FileExt == ".PDF")
                    {
                        Byte[] data = new byte[produit.Files.ContentLength];
                        produit.Files.InputStream.Read(data, 0, produit.Files.ContentLength);
                        produit.FileName = produit.Files.FileName;
                        produit.FileContent = data;
                    }
                    string fileName = Path.GetFileNameWithoutExtension(produit.image.FileName);
                    string extension = Path.GetExtension(produit.image.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    produit.imagePath = "~/Image/Produits" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/Produits"), fileName);
                    produit.image.SaveAs(fileName);



                    ModelState.Clear();

                    db.Entry(produit).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {

            }
           
            return View(produit);
        }

        // GET: ProduitBack/Delete/5
        public JsonResult Delete(int id)
        {

            bool result = false;
            Produit u = db.Produits.Where(x => x.idProduit == id).FirstOrDefault();
            if (u != null)
            {
                db.Produits.Remove(u);
                db.SaveChanges();
                result = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
