﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using data;
using domain;

namespace web.Controllers
{
    public class UserBackController : Controller
    {
         PidevContext db = new PidevContext();

        // GET: UserBack
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: UserBack/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: UserBack/Create
        public ActionResult Create()
        {
           
            return View();
        }
        // detect existed email
        [NonAction]
        public bool isEmailExist(string email)
        {
           
                var v = db.Users.Where(x => x.email == email).FirstOrDefault();
                return v != null;
           
        }

        // POST: UserBack/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "IsEmailVerified")]User user)
        {
            bool Status = false;
            string message = "";

            if (ModelState.IsValid)
            {

                var isExist = isEmailExist(user.email);
                // Email is already Exist
                if (isExist)
                {
                    //ModelState.AddModelError("EmailExist", "Email already exist");
                    message = "Email already exist";
                    return View(user);
                }
             
                user.password = Crypto.Hash(user.password);
                user.Confirmpassword = Crypto.Hash(user.Confirmpassword);
                user.role = role.admin;
                user.IsEmailVerified = false;
               

                db.Users.Add(user);
                    db.SaveChanges();
                  

                    Status = true;
                    return RedirectToAction("Index", "UserBack");
               
            }
            else
            {
                message = "Invalid Request";
            }
            ViewBag.Message = message;
            ViewBag.Status = Status;

            return View(user);
        }

        // GET: UserBack/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: UserBack/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idUser,nom,prenom,login,email,password,Confirmpassword,IsEmailVerified,ActivationCode,ResetPasswordCode,role")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }


        // POST: UserBack/Delete/5
        public JsonResult Delete(int id)
        {
           
            bool result = false;
            User u = db.Users.Where(x=>x.idUser == id).FirstOrDefault();
            if (u != null)
            {
                db.Users.Remove(u);
                db.SaveChanges();
                result = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
