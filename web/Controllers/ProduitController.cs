﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using data;
using domain;


namespace web.Controllers
{
    public class ProduitController : Controller
    {
        private PidevContext db = new PidevContext();

        // GET: Produit
        //[Authorize]
        public ActionResult Index()
        {
            return View(db.Produits.ToList());
        }
        public ActionResult getProdByCateg(string categ)
        {
            var listByCateg = db.Produits.Where(x => x.categorie.ToString() == categ).ToList();
            return Json(listByCateg, JsonRequestBehavior.AllowGet);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
