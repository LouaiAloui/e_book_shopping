﻿using data.Infrastructure;
using data.Infrastruture;
using domain;
using servicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service
{
    public class CommandeService : Service<Commande>, ICommandeService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork ut = new UnitOfWork(dbf);
        public CommandeService() : base(ut)
        {
        }

    }
}
