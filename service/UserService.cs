﻿using data.Infrastructure;
using data.Infrastruture;
using domain;
using servicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service
{
    public class UserService : Service<User>, IUserService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork ut = new UnitOfWork(dbf);
        public UserService() : base(ut)
        {
        }

        public User GetUserByLoginAndPassword(string login, string pwd)
        {
            var userr = GetMany(u => u.login == login && u.password == pwd).First();
            return userr;
        }
    }
}
