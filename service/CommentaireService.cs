﻿using data.Infrastructure;
using data.Infrastruture;
using domain;
using servicePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service
{
    public class CommentaireService : Service<Commentaire>, ICommentaireService
    {
    private static IDatabaseFactory dbf = new DatabaseFactory();
    private static IUnitOfWork ut = new UnitOfWork(dbf);
    public CommentaireService() : base(ut) {
    }

    }

}

