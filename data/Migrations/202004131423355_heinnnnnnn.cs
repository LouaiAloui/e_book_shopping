﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class heinnnnnnn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Produits", "FileName", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Produits", "FileName");
        }
    }
}
