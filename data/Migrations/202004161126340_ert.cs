﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ert : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "imagePath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "imagePath", c => c.String(unicode: false));
        }
    }
}
