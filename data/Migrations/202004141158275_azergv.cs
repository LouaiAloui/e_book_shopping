﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class azergv : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "imagePath", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "imagePath");
        }
    }
}
