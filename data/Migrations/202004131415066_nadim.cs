﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nadim : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Produits", "FileContent", c => c.Binary());
            DropColumn("dbo.Produits", "image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Produits", "image", c => c.Binary());
            DropColumn("dbo.Produits", "FileContent");
        }
    }
}
