﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class photo : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Produits", "image", c => c.Binary());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Produits", "image", c => c.String(unicode: false));
        }
    }
}
