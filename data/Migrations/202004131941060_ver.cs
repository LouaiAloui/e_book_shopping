﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ver : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Produits", "imagePath", c => c.String(unicode: false));
            DropColumn("dbo.Produits", "image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Produits", "image", c => c.String(unicode: false));
            DropColumn("dbo.Produits", "imagePath");
        }
    }
}
