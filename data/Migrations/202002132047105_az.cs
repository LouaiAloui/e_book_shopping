﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class az : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Commandes",
                c => new
                    {
                        idCommande = c.Int(nullable: false, identity: true),
                        etat = c.Boolean(nullable: false),
                        user_idUser = c.Int(),
                    })
                .PrimaryKey(t => t.idCommande)
                .ForeignKey("dbo.Users", t => t.user_idUser)
                .Index(t => t.user_idUser);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        idUser = c.Int(nullable: false, identity: true),
                        nom = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                        prenom = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                        login = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                        email = c.String(nullable: false, unicode: false),
                        password = c.String(nullable: false, unicode: false),
                        Confirmpassword = c.String(nullable: false, unicode: false),
                        IsEmailVerified = c.Boolean(nullable: false),
                        ActivationCode = c.Guid(nullable: false),
                        ResetPasswordCode = c.String(unicode: false),
                        role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idUser);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        iStatu = c.Int(nullable: false, identity: true),
                        sujet = c.String(unicode: false),
                        datStat = c.DateTime(nullable: false, precision: 0),
                        userOfStatId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.iStatu)
                .ForeignKey("dbo.Users", t => t.userOfStatId, cascadeDelete: true)
                .Index(t => t.userOfStatId);
            
            CreateTable(
                "dbo.Commentaires",
                c => new
                    {
                        idCom = c.Int(nullable: false, identity: true),
                        sujetCom = c.String(unicode: false),
                        datCom = c.DateTime(nullable: false, precision: 0),
                        forStatu_iStatu = c.Int(),
                        userCom_idUser = c.Int(),
                    })
                .PrimaryKey(t => t.idCom)
                .ForeignKey("dbo.Status", t => t.forStatu_iStatu)
                .ForeignKey("dbo.Users", t => t.userCom_idUser)
                .Index(t => t.forStatu_iStatu)
                .Index(t => t.userCom_idUser);
            
            CreateTable(
                "dbo.Produits",
                c => new
                    {
                        idProduit = c.Int(nullable: false, identity: true),
                        prix = c.Single(nullable: false),
                        description = c.String(unicode: false),
                        image = c.String(unicode: false),
                        dateProd = c.DateTime(nullable: false, precision: 0),
                        categorie = c.Int(nullable: false),
                        commande_idCommande = c.Int(),
                    })
                .PrimaryKey(t => t.idProduit)
                .ForeignKey("dbo.Commandes", t => t.commande_idCommande)
                .Index(t => t.commande_idCommande);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Produits", "commande_idCommande", "dbo.Commandes");
            DropForeignKey("dbo.Commentaires", "userCom_idUser", "dbo.Users");
            DropForeignKey("dbo.Commentaires", "forStatu_iStatu", "dbo.Status");
            DropForeignKey("dbo.Commandes", "user_idUser", "dbo.Users");
            DropForeignKey("dbo.Status", "userOfStatId", "dbo.Users");
            DropIndex("dbo.Produits", new[] { "commande_idCommande" });
            DropIndex("dbo.Commentaires", new[] { "userCom_idUser" });
            DropIndex("dbo.Commentaires", new[] { "forStatu_iStatu" });
            DropIndex("dbo.Status", new[] { "userOfStatId" });
            DropIndex("dbo.Commandes", new[] { "user_idUser" });
            DropTable("dbo.Produits");
            DropTable("dbo.Commentaires");
            DropTable("dbo.Status");
            DropTable("dbo.Users");
            DropTable("dbo.Commandes");
        }
    }
}
