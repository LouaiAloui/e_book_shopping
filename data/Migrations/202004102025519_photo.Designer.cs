﻿// <auto-generated />
namespace data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class photo : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(photo));
        
        string IMigrationMetadata.Id
        {
            get { return "202004102025519_photo"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
