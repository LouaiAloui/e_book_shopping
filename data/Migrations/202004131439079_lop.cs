﻿namespace data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lop : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Produits", "image", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Produits", "image");
        }
    }
}
