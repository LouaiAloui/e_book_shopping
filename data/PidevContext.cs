﻿
using domain;
using System.Data.Entity;

namespace data
{

    public class PidevContext : DbContext
    {
        public PidevContext() : base()
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Commande> Commandes { get; set; }
        public DbSet<Produit> Produits { get; set; }
        public DbSet<Commentaire> Commentaires { get; set; }
        public DbSet<Statu> Status { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
                modelBuilder.Entity<Statu>()
                .HasRequired<User>(s=>s.userOfStat)
                .WithMany(g => g.listOfStatu)
                .HasForeignKey(s => s.userOfStatId);

            
                
        }
    }
}

