﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domain
{
    public class Commentaire
    {
        [Key]
        public int idCom { get; set; }
        public string sujetCom { get; set; }
        [DataType(DataType.Date)]
        public DateTime datCom { get; set; }
        public User userCom { get; set; }
        public Statu forStatu { get; set; }

    }
}
