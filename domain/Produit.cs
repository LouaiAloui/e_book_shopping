﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace domain
{
    public enum Categorie
    {
        
        serie,examen,cour
    }
    public class Produit
    {
        [Key]
        public int idProduit { get; set; }
        public float prix { get; set; }
        public string description { get; set; }
        [DataType(DataType.Date)]
        public DateTime dateProd { get; set; }
        public byte[]FileContent { get; set; }
        [NotMapped]
        public HttpPostedFileBase Files { get; set; }
        public string imagePath { get; set; }
        [NotMapped]
        public HttpPostedFileBase image { get; set; }
        public string FileName { get; set; }
        public Categorie categorie { get; set; }
        public Commande commande { get; set; }
        
    }

    
   
}
