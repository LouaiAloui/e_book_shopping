﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domain
{
    public class Commande
    {
       [Key]
        public int idCommande { get; set; }
        public bool etat { get; set; }
        public User user { get; set; }
        public virtual ICollection<Produit> listProd { get; set; }
        
        

       
    }
}

