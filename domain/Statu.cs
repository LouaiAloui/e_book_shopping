﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domain
{
    public class Statu
    {
        [Key]
        public int iStatu { get; set; }
        public string sujet { get; set; }
        [DataType(DataType.Date)]
        public DateTime datStat { get; set; }
        public int userOfStatId  { get; set; }
        public virtual  User userOfStat { get; set; }
        public  virtual ICollection<Commentaire> listComByStat { get; set; }

        public Statu()
        {

        }
    }
}
